package com.example.braintrainer

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.graphics.Color
import android.media.MediaPlayer
import android.os.Build
import android.os.Bundle
import android.os.CountDownTimer
import android.view.View
import android.view.ViewAnimationUtils
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import kotlin.math.max

class MainActivity : AppCompatActivity() {

    private val options = mutableListOf<Int>()
    private var locationOfCorrectAnswer: Int = 0
    private var score: Int = 0
    private var total: Int = 0
    private var animZoomInAndFadeOut: Animation? = null
    private var bounce: Animation? = null

    private lateinit var mediaPlayer: MediaPlayer

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setTheme(R.style.MyTheme)
        setContentView(R.layout.activity_main)

        animZoomInAndFadeOut = AnimationUtils.loadAnimation(this, R.anim.zoom_in_and_fade_out)
        bounce = AnimationUtils.loadAnimation(this, R.anim.bounce)
        mediaPlayer = MediaPlayer.create(this, R.raw.times_up)
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    fun startGame(view: View) {
        exitReveal(findViewById(R.id.splashScreenID))
    }

    private fun getReadyScreen() {
        mainScreenID.visibility = View.INVISIBLE
        animScreen.visibility = View.VISIBLE

        val imageArray = intArrayOf(R.drawable.ic__three, R.drawable.ic__two, R.drawable.ic__one)
        var i = 0

        object : CountDownTimer(3000, 1000) {
            override fun onTick(p0: Long) {
                animTimer.setImageResource(imageArray[i])
                animTimer.startAnimation(animZoomInAndFadeOut)
                i++
            }

            override fun onFinish() {
                animScreen.visibility = View.INVISIBLE
                mainScreenID.visibility = View.VISIBLE
                newQuestion()
                newMatch(findViewById(R.id.playAgainButton))
            }

        }.start()

    }

    private fun newMatch(view: View) {
        view.visibility = View.GONE
        score = 0
        total = 0
        scoreTextView.text = "$score/$total"
        resultTextView.text = ""
        newQuestion()

        optionA.isClickable = true
        optionB.isClickable = true
        optionC.isClickable = true
        optionD.isClickable = true

        object : CountDownTimer(30100, 1000) {
            override fun onTick(p0: Long) {
                timerTextView.text = "${p0 / 1000}"
            }

            override fun onFinish() {
                resultTextView.text = "$score/$total"
                optionA.isClickable = false
                optionB.isClickable = false
                optionC.isClickable = false
                optionD.isClickable = false
                resultTextView.setTextColor(Color.parseColor("#FFFFFFFF"))
                resultTextView.text = "Time's Up! \n You answered $score out of $total questions."
                resultTextView.startAnimation(bounce)
                mediaPlayer.start()
                view.visibility = View.VISIBLE
            }

        }.start()
    }

    fun optionButton(view: View) {
        if (locationOfCorrectAnswer.toString() == view.tag) {
            score++
            resultTextView.setTextColor(Color.parseColor("#008F06"))
            resultTextView.text = "Correct"
        } else {
            resultTextView.setTextColor(Color.parseColor("#D60000"))
            resultTextView.text = "Wrong"
            resultTextView.startAnimation(bounce)
        }
        total++
        scoreTextView.text = "$score/$total"
        newQuestion()
    }

    private fun newQuestion() {
        val rand = java.util.Random()
        val a = rand.nextInt(21)
        val b = rand.nextInt(21)

        sumTextView.text = ("$a + $b")

        locationOfCorrectAnswer = rand.nextInt(4)

        options.clear()
        for (i in 0 until 4) {
            if (i == locationOfCorrectAnswer) {
                options.add(a + b)
            } else {
                var wrongAnswer = rand.nextInt(41)
                while (wrongAnswer == a + b) {
                    wrongAnswer = rand.nextInt(41)
                }
                options.add(wrongAnswer)
            }
        }
        optionA.text = options[0].toString()
        optionB.text = options[1].toString()
        optionC.text = options[2].toString()
        optionD.text = options[3].toString()
    }

    fun playAgain(view: View) {
        getReadyScreen()
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    private fun enterReveal(myView: View) {
        // get the center for the clipping circle
        val cx = myView.measuredWidth / 2
        val cy = myView.measuredHeight / 2

        // get the final radius for the clipping circle
        val finalRadius = max(myView.width, myView.height) / 2

        // create the animator for this view (the start radius is zero)
        val anim =
            ViewAnimationUtils.createCircularReveal(myView, cx, cy, 0f, finalRadius.toFloat())

        // make the view visible and start the animation
        myView.visibility = View.VISIBLE
        anim.duration = 1500
        anim.start()
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    private fun exitReveal(myView: View) {
        // get the center for the clipping circle
        val cx = myView.measuredWidth / 2
        val cy = myView.measuredHeight / 2

        // get the initial radius for the clipping circle
        val initialRadius = myView.width

        // create the animation (the final radius is zero)
        val anim =
            ViewAnimationUtils.createCircularReveal(myView, cx, cy, initialRadius.toFloat(), 0f)

        // make the view invisible when the animation is done
        anim.addListener(object : AnimatorListenerAdapter() {
            override fun onAnimationEnd(animation: Animator) {
                super.onAnimationEnd(animation)
                myView.visibility = View.INVISIBLE
                getReadyScreen()
            }
        })

        // start the animation
        anim.duration = 1500
        anim.start()
    }


}